package questions

import org.apache.spark.sql.SparkSession
import org.apache.spark.rdd.RDD

import scala.math._
import org.apache.spark.graphx._
import org.apache.spark.graphx.Edge
import org.apache.spark.graphx.Graph


/** GeoProcessor provides functionalites to 
  * process country/city/location data.
  * We are using data from http://download.geonames.org/export/dump/
  * which is licensed under creative commons 3.0 http://creativecommons.org/licenses/by/3.0/
  *
  * @param spark    reference to SparkSession
  * @param filePath path to file that should be modified
  */
class GeoProcessor(spark: SparkSession, filePath: String) {

  //read the file and create an RDD
  //DO NOT EDIT
  val file = spark.sparkContext.textFile(filePath)

  /** filterData removes unnecessary fields and splits the data so
    * that the RDD looks like RDD(Array("<name>","<countryCode>","<dem>"),...))
    * Fields to include:
    *   - name
    *   - countryCode
    *   - dem (digital elevation model)
    *
    * @return RDD containing filtered location data. There should be an Array for each location
    */
  def filterData(data: RDD[String]): RDD[Array[String]] = {
    /* hint: you can first split each line into an array.
    * Columns are separated by tab ('\t') character.
    * Finally you should take the appropriate fields.
    * Function zipWithIndex might be useful.
    */
    data.map((s: String) => {
      val splitted = s.split("\t")
      Array(splitted(1), splitted(8), splitted(16))
    })
  }


  /** filterElevation is used to filter to given countryCode
    * and return RDD containing only elevation(dem) information
    *
    * @param countryCode code e.g(AD)
    * @param data        an RDD containing multiple Array[<name>, <countryCode>, <dem>]
    * @return RDD containing only elevation information
    */
  def filterElevation(countryCode: String, data: RDD[Array[String]]): RDD[Int] = {
    data
      .filter((a: Array[String]) => a(1).equals(countryCode))
      .map((a: Array[String]) => a(2).toInt)
  }


  /** elevationAverage calculates the elevation(dem) average
    * to specific dataset.
    *
    * @param data : RDD containing only elevation information
    * @return The average elevation
    */
  def elevationAverage(data: RDD[Int]): Double = {
    data.map(i => i.toDouble).reduce((i1, i2) => i1 + i2) / data.count()
  }

  /** mostCommonWords calculates what is the most common
    * word in place names and returns an RDD[(String,Int)]
    * You can assume that words are separated by a single space ' '.
    *
    * @param data an RDD containing multiple Array[<name>, <countryCode>, <dem>]
    * @return RDD[(String,Int)] where string is the word and Int number of
    * occurrences. RDD should be in descending order (sorted by number of occurrences).
    * e.g ("hotel", 234), ("airport", 120), ("new", 12)
    */
  def mostCommonWords(data: RDD[Array[String]]): RDD[(String, Int)] = {
    data
      .flatMap((s: Array[String]) => s(0).split(" "))
      .map((s: String) => (s, 1))
      .reduceByKey((i1: Int, i2: Int) => i1 + i2)
      .sortBy(f => f._2, ascending = false)
  }

  /** mostCommonCountry tells which country has the most
    * entries in geolocation data. The correct name for specific
    * countrycode can be found from countrycodes.csv.
    *
    * @param data filtered geoLocation data
    * @param path to countrycode.csv file
    * @return most common country as String e.g Finland or empty string "" if countrycodes.csv
    *         doesn't have that entry.
    */
  def mostCommonCountry(data: RDD[Array[String]], path: String): String = {
    // Retrieve the country code with the most occurrences
    val countryCode = data
      .map(f => (f(1), 1))
      .reduceByKey((i1: Int, i2: Int) => i1 + i2)
      .reduce((a: (String, Int), b: (String, Int)) => if (a._2 > b._2) a else b)
      ._1

    // Load the countrycodes csv
    spark
      .read
      .option("inferSchema", value = true)
      .option("header", value = true)
      .csv(path)
      .createOrReplaceTempView("data")

    // Retrieve the name of the country by its country code.
    val result = spark.sql("select Name from data where Code = \"" + countryCode + "\" limit 1")
    if (result.count() == 0) { // Check if the country code was found.
      "" // Return the empty string if the country code was not found.
    } else {
      result.first()(0).toString // Return the name of the matched country.
    }
  }

  /**
    * How many hotels are within 10 km (<=10000.0) from
    * given latitude and longitude?
    * https://en.wikipedia.org/wiki/Haversine_formula
    * earth radius is 6371e3 meters.
    *
    * Location is a hotel if the name contains the word 'hotel'.
    * Don't use feature code field!
    *
    * Important
    * if you want to use helper functions, use variables as
    * functions, e.g
    * val distance = (a: Double) => {...}
    *
    * @param lat  latitude as Double
    * @param long longitude as Double
    * @return number of hotels in area
    */
  def hotelsInArea(lat: Double, long: Double): Int = {
    val toRadians = (decimalDegree: Double) => decimalDegree * Math.PI / 180d
    val haversineDistance = (lat1: Double, long1: Double, lat2: Double, long2: Double, radius: Double) => {
      2 * radius * asin(
        sqrt(
          pow(sin((lat2 - lat1) / 2), 2)
            + cos(lat1) * cos(lat2) * pow(sin((long2 - long1) / 2), 2)
        )
      )
    }

    file
      // Clean the data, extract the columns name, latitude and longitude
      .map((s: String) => {
        val splitted = s.split("\t")
        (splitted(1), splitted(4), splitted(5))
      })
      // Get only the locations which name contains hotel
      .filter(l => l._1.toLowerCase.contains("hotel"))
      // Calculate the distance from each hotel to the given coordinate
      .map(l => {
        val lat1 = toRadians(lat)
        val long1 = toRadians(long)
        val lat2 = toRadians(l._2.toDouble)
        val long2 = toRadians(l._3.toDouble)
        val radiusEarth = 6371e3

        haversineDistance(lat1, long1, lat2, long2, radiusEarth)
      })
      .filter(distance => distance <= 10000) // Filter based on distance
      .count().toInt // Get the amount of hotels
  }

  //GraphX exercises

  /**
    * Load FourSquare social graph data, create a
    * graphx graph and return it.
    * Use user id as vertex id and vertex attribute.
    * Use number of unique connections between users as edge weight.
    * E.g
    * ---------------------
    * | user_id | dest_id |
    * ---------------------
    * |    1    |    2    |
    * |    1    |    2    |
    * |    2    |    1    |
    * |    1    |    3    |
    * |    2    |    3    |
    * ---------------------
    * || ||
    * || ||
    * \   /
    * \ /
    * +
    *
    * _ 3 _
    * /' '\
    * (1)  (1)
    * /      \
    * 1--(2)--->2
    * \       /
    * \-(1)-/
    *
    * Hints:
    *  - Regex is extremely useful when parsing the data in this case.
    *  - http://spark.apache.org/docs/latest/graphx-programming-guide.html
    *
    * @param path to file. You can find the dataset
    *             from the resources folder
    * @return graphx graph
    *
    */
  def loadSocial(path: String): Graph[Int, Int] = {
    val pattern = "^\\s+([0-9]+)\\s*\\|\\s*([0-9]+)\\s*$"
    val socialData = spark.sparkContext.textFile(path)

    // Retrieve the edges
    val edges: RDD[Edge[Int]] = socialData
      .filter((s: String) => { // Remove all lines which do not match
        s matches pattern
      })
      .map((s: String) => { // Retrieve the values of each edge from the matched lines
        var first, second = 0l

        for {
          matched <- pattern.r findAllIn s matchData
        } {
          first = matched.group(1).toLong
          second = matched.group(2).toLong
        }

        ((first, second), 1)
      })
      .reduceByKey((i1: Int, i2: Int) => i1 + i2)
      .map((d: ((Long, Long), Int)) => Edge(d._1._1, d._1._2, d._2))

    val vertices: RDD[(Long, Int)] = edges
      .flatMap(t => Array(t.srcId, t.dstId))
      .distinct
      .map(id => (id, id.toInt))

    Graph(vertices, edges)
  }

  /**
    * Which user has the most outward connections.
    *
    * @param graph graphx graph containing the data
    * @return vertex_id as Int
    */
  def mostActiveUser(graph: Graph[Int, Int]): Int = {
    graph
      .outDegrees
      .reduce((a: (VertexId, Int), b: (VertexId, Int)) => if (a._2 > b._2) a else b)
      ._1.toInt
  }

  /**
    * Which user has the highest pageRank.
    * https://en.wikipedia.org/wiki/PageRank
    *
    * @param graph graphx graph containing the data
    * @return user with highest pageRank
    */
  def pageRankHighest(graph: Graph[Int, Int]): Int = {
    graph
      .pageRank(0.001)
      .vertices
      .reduce((a, b) => if (a._2 > b._2) a else b)
      ._1.toInt
  }
}

/**
  *
  * Change the student id
  */
object GeoProcessor {
  val studentId = "727684"
}