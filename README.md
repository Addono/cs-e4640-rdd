# CS-E4640 - RDD
Solution for the RDD assignment of CS-E4640 Big Data Platforms taught at Aalto University during the fall semester of 2018.

The assignment regards using Resilient Distributed Datasets (RDDs) to process large datasets in Apache Spark.